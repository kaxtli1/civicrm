# Oscar Pérez

##############################
FROM registry.conexo.mx/kaxtli/php-fpm:7.4 as base

RUN apt update \

 # Add PHP modules
 && apt install -y \
	php-bcmath \
	php-curl \
	php-xml \
	php-mbstring \
	php-zip \
	php-intl \
	php-fileinfo \
	php-soap \

	php-gd \

	php-mysql \

	curl \

 # Configure PHP
 && sed 's/memory_limit.*/memory_limit = 256M/'		-i /etc/php/7.4/fpm/php.ini \
 && sed 's/max_execution_time.*/max_execution_time = 240/'	-i /etc/php/7.4/fpm/php.ini \
 && sed 's/max_input_time.*/max_input_time = 120/'		-i /etc/php/7.4/fpm/php.ini \
 && sed 's/post_max_size.*/post_max_size = 50M/'		-i /etc/php/7.4/fpm/php.ini \
 && sed 's/upload_max_filesize.*/upload_max_filesize = 50M/'	-i /etc/php/7.4/fpm/php.ini \

 # Install cv
 && curl -LsS https://download.civicrm.org/cv/cv.phar -o /usr/local/bin/cv \
 && chmod +x /usr/local/bin/cv
 

##############################
FROM base  as builder

RUN apt update \

 # Install building tools
 && apt install -y --no-install-recommends composer git unzip html2text \
 && mkdir -p /var/ww/civicrm \

 # Install civicrm
 && git clone https://github.com/civicrm/civicrm-standalone /var/www/civicrm \
 && cd /var/www/civicrm \
 && composer update


##############################
# Final image
FROM base 

COPY --from=builder /var/www/civicrm /var/www/civicrm

COPY rootfs/ /

WORKDIR /var/www/civicrm

