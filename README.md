# Kaxtli CiviCRM

This is a Work In Progress ... :)


## Presentation



`kaxtli` is a collection of containers based on Debian. 

	https://gitlab.com/kaxtli1




## For inpatients 

You can instantiate a CiviCRM server with:

1. Edit the `civicrm.config.yaml` file.

2. Run podman with the YAML files.
	cat civicrm.config.yaml civicrm.volumes.yaml civicrm.pods.yaml \
	  podman kube play -

	 
`podman kube` is an alternative to `docker-compose`. You have a Kubernetes YAML compatible definiton file for pods, containers and volumes.



# Design

This work is based on the next ideas.



## PHP-FPM unix socket 

[WIP]

To get the better of containers, we need to run only one process/service per container.
We gain a lot of flexibility, maintainability and security. However, we may lost performance.

**podman** allow us to run more than one container in a pod **sharing an unix domain socket**.
By running an `nginx` and `php-fpm` in the same pod, 
we get the performance of a local unix socket without having to go up and down the network stack.

CiviCRM is a good example for this.
In the same pod, two containers are running,
one with PHP 7.4 on Debian 10 and one with nginx 1.22.1 on Debian 11. 
Sharing `/run/php/php7.4-fpm.sock` for interconection.


## One process one Container

Keep the container definition as simple as possible. Follow the UNIX philosophy, **do one thing well**


## Vertical vs Horizontal Configuration

Usually, many people define containers with all the code necessary to initialize it. At instantiation time, the container run the initialization process before be ready to launch the process of interest.

This break the **separation of concerns** principle. This project is based on the idea that:

**The initialization process is not a task for the container**

The installer does't need to be part of the container. Kubernetes resolve this with `initContainers` and podman 4.0 can handle it.

We have to process:

**Vertical Configuration** It's all the configuration than the app maintainer write in the Containerfile/Docker file, and is part of the `inmutable` behavior of any container. It's vertical because it's done 'up', and downloaded with the container image.

**Horizontal Configuration** It's the **contextual** configuration. This process run at instantiation time, and  the sysAdmin/SRE is responsible for make the neccesary adjustments. It's called horizontal because run in the production place, after the image was downloaded.

In this CiviCRM implementation this separation is clear.

The __vertical configuration__ is done in the `Containerfile/Dockerfile`, usualy none of this is of primary concerns of the production environment administrator.

	##############################
	FROM registry.conexo.mx/kaxtli/php-fpm:7.4 as base
	
	RUN apt update \
	
	 # Add PHP modules
	 && apt install -y \
		php-bcmath \
		php-curl \
		php-xml \
		php-mbstring \
		php-zip \
		php-intl \
		php-fileinfo \
		php-soap \
		php-gd \	
		php-mysql \	
		curl \
	
	 # Configure PHP
	 && sed 's/memory_limit.*/memory_limit = 256M/'		-i /etc/php/7.4/fpm/php.ini \
	 && sed 's/max_execution_time.*/max_execution_time = 240/'	-i /etc/php/7.4/fpm/php.ini \
	 && sed 's/max_input_time.*/max_input_time = 120/'		-i /etc/php/7.4/fpm/php.ini \
	 && sed 's/post_max_size.*/post_max_size = 50M/'		-i /etc/php/7.4/fpm/php.ini \
	 && sed 's/upload_max_filesize.*/upload_max_filesize = 50M/'	-i /etc/php/7.4/fpm/php.ini \
	
	 # Install cv
	 && curl -LsS https://download.civicrm.org/cv/cv.phar -o /usr/local/bin/cv \
	 && chmod +x /usr/local/bin/cv
	 
	
	##############################
	FROM base  as builder
	
	RUN apt update \
	
	 # Install building tools
	 && apt install -y --no-install-recommends composer git unzip html2text \
	 && mkdir -p /var/ww/civicrm \
	
	 # Install civicrm
	 && git clone https://github.com/civicrm/civicrm-standalone /var/www/civicrm \
	 && cd /var/www/civicrm \
	 && composer update
	
	
	##############################
	# Final image
	FROM base 
	
	COPY --from=builder /var/www/civicrm /var/www/civicrm
	
	COPY rootfs/ /
	
	WORKDIR /var/www/civicrm
	

The __horizontal configuration__ is done in the `k8s YAML` file. The initContainer define the setup proccess that is in concern of the sysadmin/sre. Ideally they only need to define some variables in the config.yaml file, but if they can read the code, can understand better what is possible to do. 

	initContainers:
	  - image: registry.conexo.mx/kaxtli/civicrm:beta
	    name: initCivicrm
	    envFrom:
	      - configMapRef:
	         name: civicrm-vars
	    command: [ "bash-args"]
	    args:
	      - |-
	        set -e
	        mkdir -p /run/php
	        mkdir -p /var/www/civicrm/web/upload/
	        chown www-data:www-data /var/www/civicrm/data /var/www/civicrm/web/upload/
	        chmod 2770 /var/www/civicrm/data /var/www/civicrm/web/upload/
	        mkdir -p /run/php
	
	        apt update
	        apt install -y sudo
	
	        cd /var/www/civicrm
	        echo -------
	        sudo -u www-data \
	           cv core:install \
	           --db=mysql://${DB_USER}:${DB_PASSWD}@civicrm-db:3306/civicrm \
	           --cms-base-url=http://${FQDN}/
	
	    resources: {}
	    securityContext:
	      capabilities:
	        drop:
	        - CAP_MKNOD
	        - CAP_NET_RAW
	        - CAP_AUDIT_WRITE
	    volumeMounts:
	
	    - mountPath: /run/
	      name: civicrm-run
	
	    - mountPath: /var/www
	      name: civicrm-var-www
	
	    - mountPath: /etc/nginx/conf.d/
	      name: civicrm-etc-nginx-confd
	
In a next version, I will add the use of `kustomization` as **configuration manager**.

## Kubernetes-like definition


This implementation do not need kubernetes, just use k8s YAML files to deploy the containers in pods with podman.

[TODO]




